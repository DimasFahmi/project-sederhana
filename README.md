# Project Sederhana

Hanya Belajar
Nama : Dimas Fahmi Suntoro
Kampus ITS Teknik Elektro

Untuk Soal Yang Dikerjakan Adalah soal 3,4,5 dan 2.
Untuk merunning file dibutuhkan JVM(Java Virtual Machine) dan yang saya Gunakan adalah type 1.8
Untuk Merunning file dibutuhkan Software Spring tool Suite atau yang sejenis dan dengan ini saya gunakan STS.


Cara Membuka File Dengan klik file di STS dan Import --> Git --> Project from Git -->Clone URL
Lalu Pastekan url GIT ke url dan yang lain akan terisi otomatis.

setelah itu klik kanan file java difolder package arcademy pilih run as --> Java Application.

Penjelasa mengenai alur program dijelaskan dalam program itu sendiri yang berbentuk comment

Untuk mencoba program di https://www.compilejava.net/...
Package dalam baris program paling atas harus dihapus karena akan error.


Soal 1 Menenai RestFul API
Apa itu API?
	RESTful API didasarkan pada teknologi state transfer (representational state transfer / REST), 
gaya arsitektur dan pendekatan komunikasi yang sering digunakan dalam pengembangan layanan web.
Meskipun REST dapat digunakan di hampir semua protokol, tapi biasanya memanfaatkan HTTP ketika 
digunakan untuk Web API. Hal ini membantu pengembang web tidak perlu menginstal library atau 
perangkat lunak tambahan untuk memanfaatkan desain REST API. Design REST API pertama kali 
diperkenalkan oleh Dr. Roy Fielding dalam disertasi doktor tahun 2000-nya. REST API terkenal 
karena fleksibilitasnya yang luar biasa. Data tidak terikat dengan metode dan sumber daya, 

	REST memiliki kemampuan untuk menangani beberapa jenis panggilan, mengembalikan format data yang 
berbeda dan bahkan mengubah secara struktural tentunya dengan implementasi yang benar.
REST yang digunakan oleh browser dapat dianggap sebagai bahasa internet. Dengan meningkatnya 
penggunaan cloud, API muncul untuk mengekspos layanan web. REST adalah pilihan logis untuk 
membangun API yang memungkinkan pengguna untuk terhubung dan berinteraksi dengan layanan cloud. 
API telah banyak digunakan oleh situs-situs seperti Amazon, Google, LinkedIn dan Twitter.

	Contoh paling sederhana adalah Google API untuk login. Anda tidak perlu menghabiskan banyak waktu 
hanya untuk membuat sistem login untuk member di website. Anda hanya perlu memanggil API dari 
google, pengguna hanya perlu login ke Google kemudian Anda akan mendapatkan data semisal alamat 
email / nama dari pengguna tersebut. Tentunya jika pengguna telah memberikan izin mengunakan data 
mereka. 

Kegunaan JSON Dalam RESTFul API
Karena RESTFul API digunakan untuk pertukaran data dalam website, maka JSON sangat diperlukan karena 
itu merupakan bahasa pemrograman untuk pertukaran data yang baik dan JSON biasanya digunakan 
sebagai format standar untuk bertukar data antar aplikasi. Untuk RESTFul API, JSON sebagai format 
untuk bertukar data client dan server atau antar aplikasi.